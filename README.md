## О проекте

Задача - развернуть веб-сайт для загрузки файлов в nfs-хранилище.

## Инструменты использованные в проекте:

- ![Packer](https://www.packer.io/)
- ![Vault](https://www.vaultproject.io/)
- ![Terraform](https://www.terraform.io/)
- ![Yandex cloud](https://cloud.yandex.ru/)
- ![Nginx](https://www.nginx.com/)
- ![Docker](https://www.docker.com/)
- ![Ansible](https://www.ansible.com/)


## Что тут как

### Инфраструктура

Разворачивать каждый раз все с помощью ansible - долго. Время = деньги (особенно в случае с облаком), потому экономим время, подкинув некоторые пакетики packer'ом.
Собранный packer'ом ![образ](https://gitlab.com/deusops_public/231206_infra/-/blob/master/packer/main_img.pkr.hcl) запускается terraform'ом ![main.tf](https://gitlab.com/deusops_public/231206_infra/-/blob/master/main.tf) в yandex cloud.
Все секретики (folder_id, cloud_id, image_id и yc_token) подхватываются из домашнего hashicorp vault, развернутого на raspberryPi в докере.

При деплое используются ![роли](https://galaxy.ansible.com/ui/standalone/namespaces/13083), загруженные мной в ansible-galaxy

Создается 2 ВМ:
- file01 (www.file01.shabalin.site)

Поднимаем nfs-хранилище с помощью ![роли](https://galaxy.ansible.com/ui/standalone/roles/lexshabalin/231206_nfs_server_role/). 

- web01 (www.web01.shabalin.site)

Тут используем роли для поднятия ![nfs-клиента](https://galaxy.ansible.com/ui/standalone/roles/lexshabalin/231206_nfs_client_role/), ![gitlab-runner](https://galaxy.ansible.com/ui/standalone/roles/lexshabalin/231206_gitlab_runner_role/) и ![nginx](https://galaxy.ansible.com/ui/standalone/roles/lexshabalin/231206_nginx_role/). Все одним ![плейбуком](https://gitlab.com/deusops_public/231206_service/-/blob/master/nfs_client_playbook.yml)
Web01 разворачивается только после отработки последней роли на file01, иначе dns не успевает подхватить адрес, а монтирование nfs-хранилища как раз таки идет по dns-имени.

### Сервис

После деплоя стенда вебхуком запускается сборка образа в репозитории ![service](https://gitlab.com/deusops_public/231206_service/-/tree/master). После сборки образ пушится в dockerhub (https://hub.docker.com/r/oorangebuster/filesharing/tags). 
Я оставил деплой приложения мануальным. Во время деплоя на web01 устанавливается роль ![docker](https://galaxy.ansible.com/ui/standalone/roles/lexshabalin/231206_docker_role/) c файлом docker-compose, который и запускает django-filesharing.

![Схема](https://gitlab.com/deusops_public/231206_service/raw/master/pic/scheme.jpg)
